import {
  SET_CARDS_VISIBLE,
  SET_TRIP_ID,
  SET_REFRESH_CARDS,
  REFRESH_CARDS_DONE,
  DELETE_TRIP,
  DELETE_DONE,
  UPDATE_TRIP,
  UPDATE_DONE
} from "../actions/types";
const INITIAL_STATE_CARDS = {
  cardsVisible: true,
  tripData: [],
  trip_id_selected: ""
};

export default (state = INITIAL_STATE_CARDS, action) => {
  switch (action.type) {
    case SET_REFRESH_CARDS:
      console.log("Reducer: card refresh requested " + action.payload);
      return { ...state, cardsRefreshRequested: action.payload };
    case SET_CARDS_VISIBLE:
      return { ...state, cardsVisible: action.payload };
    case SET_TRIP_ID:
      return { ...state, trip_id_selected: action.payload };
    case REFRESH_CARDS_DONE:
      return {
        ...state,
        tripData: action.payload.tripData,
        selectedId: action.payload.selectedId
      };
    case DELETE_TRIP:
      return { ...state, tripToDelete: action.payload };
    case DELETE_DONE:
      return { ...state, tripToDelete: undefined };
    case UPDATE_TRIP:
      return { ...state, tripToUpdate: action.payload };
    case UPDATE_DONE:
      return { ...state, tripToUpdate: undefined };

    default:
      return state;
  }
};
